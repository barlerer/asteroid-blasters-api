import express from "express"
import cors from "cors";
import helmet from "helmet";
import users from "../routes/users"
import compression from "compression"
import * as dotenv from "dotenv";
dotenv.config();

const app = express();

app.use(compression());
app.use(helmet());
app.use(cors());
app.use(express.json());

//routes
app.use('/users', users)

export default app