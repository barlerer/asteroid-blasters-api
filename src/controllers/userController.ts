import * as express from "express";
import * as userValidator from "../validation/userValidator"
import db from '../db/database'
import * as scoreValidator from "../validation/scoreValidator"
import * as encryptor from "../encryption/encryptor"

export async function getUser(req:express.Request, res: express.Response, next: express.NextFunction) {
    const userId = req.params.id
    try {
        const result = await findUserById(userId)
        if  (result.rowCount === 0) {
           return res.status(404).send()
        }
        return res.send(result.rows[0])
    } catch (error) {
        return res.status(500).send()
    }
}

export async function saveUser(req:express.Request, res: express.Response, next: express.NextFunction) {
    let user = req.body
    const {err} = userValidator.validateUser(user)
    if (err) {
       return res.status(400).send(err.details[0].message)
    }
    try {
        user.password = await encryptor.encrypt(user)
        await putUser(user)
        return res.send(user)
    } catch (error) {
       return res.status(400).send("email is taken")
    }

}

export async function saveUserScore(req:express.Request, res: express.Response, next: express.NextFunction) {
    const score = req.body
    const userId = req.params.id
    const {error} = scoreValidator.validateScore(score)
    if (error) {
        return res.status(400).send(error.details[0].message)
    }
    try {
        const user = await findUserById(userId)
        if (user.rowCount === 0) {
            return res.status(404).send("No user found") 
        }
        let userWithScore = await saveScore(score, userId)
        userWithScore = await getUserWithAllScores(userId)
        return res.send(userWithScore.rows)
    } catch (error) {
        return res.status(500).send()
    }
}

export async function signIn(req:express.Request, res: express.Response, next: express.NextFunction) {
    const user = req.body
    const {error} = userValidator.validateUser(user)
    if (error) {
        return res.status(400).send(error.details[0].message)
    }
    //We need to find password, so we have email, get the password
    let userFromDB = await findUserByEmail(user)
    if (userFromDB.rowCount === 0) {
        return res.status(404).send()
    }
    const result = await encryptor.match(user, userFromDB.rows[0].password)
    if (!result) {
        return res.status(401).send()
    }
    return res.send()
}

async function findUserById(id:string) {
    const result = await db.query('SELECT * FROM users WHERE id = $1', [id])
    return result
}

async function findUserByEmail(user) {
    const result = await db.query('SELECT * FROM users WHERE email = $1', [user.email])
    return result
}

async function putUser(user) {
    const res = await db.query(`INSERT INTO users (email, password) VALUES ($1, $2);`, [user.email, user.password])
    return res
}

async function saveScore(score, userId) {
    const res = await db.query(`INSERT INTO scores (user_id, score, nickname) VALUES ($1, $2, $3);`, [userId, score.score, score.nickname])
    return res
}

async function getUserWithAllScores(userId) {
    const res = await db.query(`SELECT id, email, score, nickname from users INNER JOIN scores on user_id=id WHERE id=$1 ORDER BY score DESC;`, [userId])
    return res
}