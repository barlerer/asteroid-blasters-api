import Joi from "@hapi/joi"

const userSchema = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().min(5).required()
})

export function validateUser(user:any) {
    return userSchema.validate(user)
}