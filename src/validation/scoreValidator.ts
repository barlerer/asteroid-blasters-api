import Joi from "@hapi/joi"

const scoreSchema = Joi.object({
    score: Joi.number().min(0),
    nickname: Joi.string()
})

export function validateScore(score:any) {
    return scoreSchema.validate(score)
}