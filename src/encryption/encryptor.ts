import * as bcrypt from "bcrypt"
const saltRounds = 8;

export async function encrypt(user:any) {
    return await bcrypt.hash(user.password, saltRounds)
}

export async function match(user:any, passwordToCheck: string) {
    return await bcrypt.compare(user.password, passwordToCheck)
}