import express from 'express'
import db from '../db/database'
import * as userController from "../controllers/userController"
const router = express.Router()

router.get('/login',userController.signIn)
router.get('/:id',userController.getUser)
router.post('/',userController.saveUser)
router.post('/:id',userController.saveUserScore)


export default router