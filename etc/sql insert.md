CREATE TABLE "users" (
	"id" serial NOT NULL,
	"email" varchar(255) NOT NULL UNIQUE,
	"password" varchar(255) NOT NULL,
	CONSTRAINT "users_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "scores" (
	"score_id" serial NOT NULL,
	"user_id" int NOT NULL,
	"score" int NOT NULL,
	"nickname" varchar(45) NOT NULL,
	CONSTRAINT "scores_pk" PRIMARY KEY ("score_id")
) WITH (
  OIDS=FALSE
);




ALTER TABLE "scores" ADD CONSTRAINT "scores_fk0" FOREIGN KEY ("user_id") REFERENCES "users"("id");


INSERT INTO users (email, password) VALUES ('test@gmail.com', '123456');

INSERT INTO scores (user_id, score, nickname) VALUES ('1', '30', 'Anon2');
