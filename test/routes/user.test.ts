const request = require('supertest')
import app from "../../src/app/app"
import db from "../../src/db/database"

const user = {
  email: "barlerer@gmail.com",
  password: "12345"
}

const score = {
  score: 100,
  nickname: "AnonTesting"
}

beforeAll(async () => {
  await dropTablesUsersDatabase();
});

beforeEach(async () => {
  await dropTablesUsersDatabase();
  await createTables()
});


async function createTables() {
  const text = `CREATE TABLE users (
    id serial NOT NULL,
    email varchar(255) NOT NULL UNIQUE,
    password varchar(255) NOT NULL,
    CONSTRAINT users_pk PRIMARY KEY (id)
  ) WITH (
    OIDS=FALSE
  );
  
  CREATE TABLE "scores" (
    "score_id" serial NOT NULL,
    "user_id" int NOT NULL,
    "score" int NOT NULL,
    "nickname" varchar(45) NOT NULL,
    CONSTRAINT "scores_pk" PRIMARY KEY ("score_id")
  ) WITH (
    OIDS=FALSE
  );`
 
  await db.query(text, [])
}

async function dropTablesUsersDatabase() {
  await db.query("DROP TABLE IF EXISTS scores;", [])
  await db.query("DROP TABLE IF EXISTS users;", [])
}

describe('get Endpoints', () => {

  it('Should return an error since user is not in the system', async (done)=> {
    const res = await request(app)
      .get('/users/3')
    expect(res.statusCode).toEqual(404)
    done();
  })
})

describe("Post end points", () => {
  it("Should return the user posted", async (done) => {
    const res = await request(app).post('/users').send(user)
    expect(res.body.email).toMatch(user.email)
    done()
  })

  it("Should return 404 as user is already exist in the system", async (done) => {
    await request(app).post('/users').send(user)
    const res = await request(app).post('/users').send(user)
    expect(res.status).toBe(400)
    done()
  })
})

describe("Post and get", () => {
  it("Should return the user posted", async (done) => {
    await request(app).post('/users').send(user)
    const res = await request(app)
      .get(`/users/1`)
    expect(res.statusCode).toEqual(200)
    expect(res.body.email).toMatch(user.email)
    expect(res.body.password === user.password).toBeFalsy()
    done();
  })
})
describe("Post for scores", () => {
  it("Should save the score for a user", async (done) => {
    await request(app).post('/users').send(user)
    const result = await request(app)
    .post('/users/1').send(score)
    expect(result.status).toBe(200)
    done()
  })

  it("Should return an error since the user does not exist yet", async (done) => {
    const result = await request(app)
    .post('/users/100').send(score)
    expect(result.status).toBe(404)
    done()
  })

  it("Should save the score for a user", async (done) => {
    await request(app).post('/users').send(user)
    const result = await request(app)
    .post('/users/1').send(score)
    expect(result.body[0]).toHaveProperty("email", user.email)
    expect(result.body[0]).toMatchObject(score)
    done()
  })

  it("Should return 400 as score is negative", async (done) => {
    let badScore = {
      score: -5,
      nickname: "Test"
    }
    await request(app).post('/users').send(user)
    const result = await request(app)
    .post('/users/1').send(badScore)
    expect(result.status).toBe(400)
    expect(result.text).toMatch("score")
    done()
  })
})

describe("Login", ()=> {
  it("Should log in", async()=> {
    await request(app).post('/users').send(user)
    const result = await request(app)
    .get('/users/login').send(user)
    expect(result.status).toBe(200)
  })

  it("Should not log in", async()=> {
    let badUser = {
      email: "barlerer@gmail.com",
      password: "123456"
    }
    await request(app).post('/users').send(user)
    const result = await request(app)
    .get('/users/login').send(badUser)
    expect(result.status).toBe(401)
  })
})