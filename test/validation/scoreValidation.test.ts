import * as scoreValidator from "../../src/validation/scoreValidator"

const score = {
    score: 15,
    nickname: "Testing"
}

describe("score validation testing", ()=> {
    it("Should say score is good",async ()=> {
        const {error} = await scoreValidator.validateScore(score)
        expect(error).toBeUndefined()
    })

    it("Should say score is not good",async ()=> {
        let badScore = {
            score: -15,
            nickname: "Testing"
        }
        const {error} = await scoreValidator.validateScore(badScore)
        expect(error).toBeDefined()
        expect(error.details[0].message).toMatch("score")
    })
})