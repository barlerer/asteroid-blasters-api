import * as userValidator from "../../src/validation/userValidator"

let user

beforeEach(()=> {
    user = {
        email: "testing@gmail.com",
        password: "!23455"
    }
})

describe("User validation", ()=> {

    it("Should return no error", ()=> {
        const {error} = userValidator.validateUser(user)
        expect(error).toBeUndefined()
    })

    it("Should return error because email is invalid", ()=> {
        user.email = "w"
        const {error} = userValidator.validateUser(user)
        expect(error.details[0].message).toMatch("email")
    })

    it("Should return error because password is invalid", ()=> {
        user.password = "1"
        const {error} = userValidator.validateUser(user)
        expect(error.details[0].message).toMatch("password")
    })
})
