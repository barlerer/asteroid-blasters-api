import * as encryptor from "../src/encryption/encryptor"

const user = {
    email: "test@testing.com",
    password: "12345"
}

describe("bCrypt suite", ()=> {
    it("Should crypt the password", async ()=> {
        const crypt = await encryptor.encrypt(user)
        expect(crypt === user.password).toBeFalsy()
    })

    it("Should dcrypt the password", async ()=> {
        const crypt = await encryptor.encrypt(user)
        const res = await encryptor.match(user, crypt)
        expect(res).toBeTruthy()
    })
})